use 5.018;
use Test::More ;
use Brainbuz::Utility;

BEGIN {use_ok( 'Brainbuz::Utility' );};


my @TESTS = (
    [ qq |<head>
<title>Kimmel Center: Ax, from Bach to Strauss</title>
<script language="JavaScript" src="/js/2010.js"></script> |,
    qq | \n Kimmel Center: Ax, from Bach to Strauss \n   | ],
    [ qq |<table cellpadding="2" cellspacing="0" border="0" style="margin: 2px -2px 0px -2px;">
<tr valign="top"><td><b>Sunday, April 13</b></td><td><b>8pm</b></td><td><a href="https://tickets.kimmelcenter.org/tickets/reserve.aspx?performanceNumber=20362" class="buybtn" target="_blank">buy tickets!</a></td></tr>| ,
    qq | \n   Sunday, April 13    8pm    buy tickets!   | ],
    ) ;

foreach my $T (@TESTS) {
    my ( $in, $out ) = ( $T->[0], $T->[1] ) ;
    my $result = htmlstrip( $in ) ;
    is( $result, $out, "stripped version is *$result*" );
    }
    
done_testing();
