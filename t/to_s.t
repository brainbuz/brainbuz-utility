use Test::More ;

BEGIN {use_ok( Brainbuz::Utility );};

package OBJ;

sub new { 
	my $self = {} ;
	$self->{data} = 'Mary Poppins' ;
	bless $self ;
	return $self ;
	}
sub  blab {
	my $self = shift ;
	$self->{data} ;
	}
sub consume {
	my $self = shift ;
	$self->{data} = shift ;
	}

package Main;

use Test::More ;
use Brainbuz::Utility;

# I can't figure out how to force misconstruation as a reference!

my $obj = new OBJ ;
#$obj->consume( $obj->blab ) ;
#isnt( $obj->blab , 'Mary Poppins');
#note( "$obj->blab" );
is( to_s($obj->blab), 'Mary Poppins', 'make sure result is stringified');

done_testing();
