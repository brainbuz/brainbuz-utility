package Brainbuz::Utility;

our $VERSION = '14.0201';

use Exporter 'import';    # gives you Exporter's import() method directly
@EXPORT = qw( to_s htmlstrip sort_hash );    # symbols to export on request

# ABSTRACT: Brainbuz Utility Functions.

=head1 VERSION 

14.0201

=head1 NAME

Brainbuz Utility Functions

=cut

sub to_s {
    my $str = shift;
    return sprintf( '%s', $str );
}

sub htmlstrip {
    my $h = shift;
    $h =~ s/<[^>]*>/ /gs;
    return $h;
}

sub sort_hash {
    my %H      = @_;
    my @sorted = ();
    $direction = delete $H{direction} || 'asc';
    $alpha     = delete $H{alpha}     || 0;
    $numeric   = delete $H{numeric}   || 1;
    if ( defined $H{hashref} ) { %H = %{ $H{hashref} } }
    if ($alpha) {
        foreach my $name ( sort { lc $H{$a} cmp lc $H{$b} } keys %H ) {
            push @sorted, $name;    }
        }
    else {
        foreach my $name ( sort { $H{$a} <=> $H{$b} } keys %H ) {
            push @sorted, $name;
        }
    }
    if ( lc($direction) eq 'desc' ) {
        return reverse @sorted;
    }
    else { return @sorted; }
}

#http://perlmaven.com/how-to-sort-a-hash-in-perl

1;

=pod 

=head1 SYNOPSIS 

Import Brainbuz' little snippets into your namespace. 

    use Brainbuz::Utility; # imports all

=head2 to_s

In Ruby it is frequently necessary to instruct an object to provide a certain representation of itself. In Perl this is usually not necessary, but sometimes it when passing the output of a method to another object's method a reference gets passed instead of the output. In this situation putting the object in "quotes" won't work, and the obvious code using sprint is kind of ugly. Assigning to a variable first requires an extra, so for at least the most common case of needing a string, I have to_s.

=head2 htmlstrip

There are several modules for stripping html, but there are also some simpler regexes that work well most of the time. This method implements =~ s/<[^>]*>/ /gs, replacing tags with spaces. 

=head2 sort_hash 

Return a sorted array containing the keys of a hash.

 my @sorted = sort_hash( 
    direction => 'desc' , # default is asc
    alpha     => 1 , # Sort alpha, default is numeric
    numeric   => 1, # sort as numbers.
    hashref   => $hashref , # pass a hashref instead of a hash.
    );

Args are passed in with the hash, keys of direction, alpha, and
numeric would be processed as arguments. To avoid this use a 
hashref instead.

=cut

=head1 AUTHOR

John Karr, C<< <brainbuz at brainbuz.org> >>

=head1 BUGS

Please don't report any bugs. 

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

You can also look for information at:

=head1 LICENSE AND COPYRIGHT

Copyright 2014 John Karr.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3 or at your option
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available in the source tree;
if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

=cut
